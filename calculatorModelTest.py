from calculatorController import menuInput
from calculatorModel import addFunction, subFunction, mulFunction, divFunction


def test_menuInput_end():
    test_value = 5
    assert(menuInput(test_value)) is True
    print("test_menuInput_end: Tests passed")


def test_addFunction():
    test_num1 = 5
    test_num2 = 7
    test_num3 = 8
    test_num4 = 24

    assert(addFunction(test_num1, test_num2)) == 12
    assert(addFunction(test_num3, test_num4)) == 32
    print('test_addFunction: Tests passed')


def test_subFunction():
    test_num1 = 10
    test_num2 = 5
    test_num3 = 6
    test_num4 = 12

    assert(subFunction(test_num1, test_num2)) == 5
    assert(subFunction(test_num3, test_num4)) == -6
    print("test_subFunction: Tests passed")


def test_mulFunction():
    test_num1 = 10
    test_num2 = 5
    test_num3 = -6
    test_num4 = 12

    assert(mulFunction(test_num1, test_num2)) == 50
    assert(mulFunction(test_num3, test_num4)) == -72
    print("test_mulFunction: Tests passed")


def test_divFunction():
    test_num1 = 10
    test_num2 = 5
    test_num3 = -6
    test_num4 = 12

    assert(divFunction(test_num1, test_num2)) == 2
    assert(divFunction(test_num3, test_num4)) == -0.5
    print("test_mulFunction: Tests passed")


if __name__ == '__main__':
    test_menuInput_end()
    test_addFunction()
    test_subFunction()
    test_mulFunction()
    test_divFunction()