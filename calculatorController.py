# Alfonso Miguel Santos-Tankia and Zachary Wileman Calculator Program
# CS5101

from calculatorModel import addFunction, subFunction, mulFunction, divFunction
from calculatorView import welcome, menu, getUserInput, printResult, printMenuInputError

def main():
    welcome()
    end = False

    while not end:
        menu()
        userInput = int(input(''))
        end = menuInput(userInput)


def menuInput(userInput):
    end = False
    validOptions = [1, 2, 3, 4, 5]

    if userInput in validOptions:
        if userInput == 5:
            end = True
        else:
            inputValues = getUserInput()

            if userInput == 1:
                result = addFunction(inputValues['num1'], inputValues['num2'])
            elif userInput == 2:
                result = subFunction(inputValues['num1'], inputValues['num2'])
            elif userInput == 3:
                result = mulFunction(inputValues['num1'], inputValues['num2'])
            else:  # userInput == 4
                result = divFunction(inputValues['num1'], inputValues['num2'])

            printResult(result)

    else:
        printMenuInputError()

    return end


if __name__ == '__main__':
    # start it
    main()
