def welcome():
    print('Welcome to THE Calculator Program!')


def menu():
    print("Enter the menu option you would like to do: ")
    print("1.) Add two numbers")
    print("2.) Subtract two numbers")
    print("3.) Multiply two numbers")
    print("4.) Divide two numbers")
    print("5.) End program")


def getUserInput():
    num1 = int(input('Please enter the first number: '))
    num2 = int(input('Please enter the second number: '))
    return {'num1': num1, 'num2': num2}


def printResult(result):
    print('The result of the calculation is: ' + str(result) + '\n')


def printMenuInputError():
    print('Please enter a valid menu choice')